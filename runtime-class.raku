use v6.d;
use MONKEY-SEE-NO-EVAL;

constant NAME = 'Pheix::Addons::Runtime::' ~ ((my $match = now.Rat.Str) ~~ s:g/(<[\d]>+)$//);

my $attributes = {
    date => { code => 'DateTime.now()' },
    code => { code => '(0..1).rand' },
    path => { code => '(1..99).rand.Int' },
    ctrl => { code => '(100..999).rand.Int' },
    name => { code => '"DecentralizedAddon"' },
};

my @inputs  = <var1 var2 var3>;

my $methods = {
    init => {
        code => q:to/CODE/;
%method_args.gist.say;
my $value;
(1..5).map({ ($value //= 1) *= $_ });
say self.^name ~ q{.} ~ $method_name ~ "(): $value";
CODE
    },
    build => {
        code => q:to/CODE/;
say self.^name ~ q{.} ~ $method_name ~ "()";

for self.^attributes -> $attr {
    my $attr_hash = $attr.get_value(self);

    next unless $attr_hash && $attr_hash ~~ Hash;

    %($attr_hash.kv.map({$_})).gist.say;
};
CODE
    },
};

{
    class Pheix::Addons::Runtime { has Hash $.attributes; }

    constant RUNTIMECLASS := Metamodel::ClassHOW.new_type(name => NAME);

    RUNTIMECLASS.^add_parent(Pheix::Addons::Runtime.new);

    for $methods.keys -> $method {
        RUNTIMECLASS.^add_method(
            $method, method { my %method_args = %_; my $method_name = $method; EVAL($methods{$method}<code>); return True; }
        );
    }

    RUNTIMECLASS.^compose;
}

my $runtime_class = RUNTIMECLASS.new(attributes => $attributes.kv.map({$_ ~~ Hash ?? EVAL($_<code>) !! $_}).Hash);

$methods.keys.sort.map({$runtime_class."$_"(|@inputs.kv.map({$_ ~~ Int ?? @inputs[$_] !! (1000..9999).rand.Int}).Hash)});
