# Decentralized CMS modules

Source issue: https://gitlab.com/pheix-pool/core-perl6/-/issues/130

## Overview

Very interesting idea, originally appeared at https://gitlab.com/pheix-pool/core-perl6/-/issues/120#note_516622485.

The task is to consider the case, can we do this in the some another way: I mean now we can have different instances of CMS with different modules set.

But for example, if module is the interface for smart contract instance – it's become truly decentralized.

## Further steps

Looks very interesting, but current build-in modules like `Pheix::Addons::Embedded::Admin` and `Pheix::Addons::Embedded::User` do not fit this model.

Anyway the idea do not seems like a utopia.

What we can still investigate:

1. Create a list of common module parts and separate them to `Pheix::Addons::Embedded::Framework`;
2. Specify the logic for each module and keep it as the list of static/shared subroutines;
3. Set the list of rules, parameters, whatever... to organize entities from no.1 and no.2 to functional module (seems like smth impossible 🤯);
4. Store data from no.3 to blockchain: own smart contract (each module has own smart contract or universal — no matter for current step).

**{-&nbsp;NOTE&nbsp;-}**: we can store no.3 and no.2 (real sources) on blockhain and create own installer/manager like `thez` (🎯 nice name) to install explicitly for Pheix.

## Initial steps

Just wrote the pre-RC2 product [concepts](https://gitlab.com/pheix-research/talks/-/issues/5), the very straight forward idea for the start — store module setting on blockchain.

Now we have main `config.json` and `*.json` files for embedded modules. It's quite easy to store secondary `*.json` files on blockchain — as we have `addon.group.installed` section at `config.json`, it's ok to store reference to `storage` with actual addon settings.

It could be simple `# id;name;json` table with module config.

The main `config.json` config also could be stored entirely on blockchain — now it looks static. But we have to focus on sensitive data like keys and tokens.

They should obviously stored locally. There could be the solutions with local blockchain node: we store keys and tokens there.

Initial steps:

1. App started: no modules or any functionality available;
2. We set API request to `import-settings` route with: `local endpoint`, `login`, `password`, smart contract `transaction hash` and setting `tablename` (save locally option);
3. It request is authenticated, settings are imported and applied;
4. Redirect to `index` route;
