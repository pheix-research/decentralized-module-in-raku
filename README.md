# Decentralized module in Raku

## Overview

This repository contains research on decentralized module in Raku: https://gitlab.com/pheix-pool/core-perl6/-/issues/130.

## Installer

Details about `thez` (dModules installer/manager): https://gitlab.com/pheix-research/decentralized-module-in-raku/-/tree/main/docs/arch#further-steps

## License information

This is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
